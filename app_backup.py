import os,io,re
import pandas as pd
from flask import Flask,flash,request,redirect,url_for,render_template,send_from_directory,jsonify
from werkzeug.utils import secure_filename
import tika
from tika import parser
import base64
from collections import OrderedDict
#pdfminer configuration
from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
#spacy configuration
import spacy
from spacy.matcher import Matcher,PhraseMatcher
from unidecode import unidecode
nlp = spacy.load('en_core_web_sm')
#noun_chunks = nlp.noun_chunks
matcher = Matcher(nlp.vocab)

UPLOAD_FOLDER = '/home/cyperts/cvParser/cvs/'
ALLOWED_EXTENSIONS = set(['pdf'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
'''@app.route('/')
def index():
    return render_template('index.html')
'''
def extract_name(resume_text):
    nlp_text = nlp(resume_text)
    pattern = [{'POS': 'PROPN'}, {'POS': 'PROPN'}]
    matcher.add('Name', None, pattern)
    matches = matcher(nlp_text)
    for match_id, start, end in matches:
        span = nlp_text[start:end]
        return span.text
    
def extract_mobile_number(text):
    phone = re.findall(re.compile(r'(?:(?:\+?([1-9]|[0-9][0-9]|[0-9][0-9][0-9])\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([0-9][1-9]|[0-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?'), text)
    if phone:
        number = ''.join(phone[0])
        if len(number) > 10:
            return '+' + number
        else:
            return number
        
def extract_email(text):
    email = re.findall("([^@|\s]+@[^@]+\.[^@|\s]+)", text)
    if email:
        try:
            return email[0].split()[0].strip(';')
        except IndexError:
            return None
        
def extract_skills(text):
    nlp_text = nlp(text)
    noun_chunks = (nlp_text.noun_chunks)
    #removing stop words and word tokenization
    tokens = [token.text for token in nlp_text if not token.is_stop]
    #reading the csv file
    data = pd.read_csv(os.path.join(os.path.dirname(__file__), 'skills.csv')) 
    #extract values
    skills = list(data.columns.values)
    skillset = []
    #check for one-grams 
    for token in tokens:
        if token.lower() in skills:
            skillset.append(token)
    #check for bi-grams and tri-grams
    for token in noun_chunks:
        token = token.text.lower().strip()
        if token in skills:
            skillset.append(token)
    return [i.capitalize() for i in set([i.lower() for i in skillset])]

def extract_designations(text):
    nlp_text = nlp(text)
    designation_set = []
    result=[]
    designations = []
    data = pd.read_csv(os.path.join(os.path.dirname(__file__), 'designations.csv')) 
    designations = list(data.columns.values)
    pattern1 = [{'POS': 'NOUN'}]
    pattern2 = [{'POS': 'NOUN'}, {'POS': 'ADJ'}]
    pattern3 = [{'POS': 'ADJ'},{'POS': 'NOUN'},{'POS': 'NOUN'}]
    matcher.add('Designations',None,pattern3,pattern2,pattern1)
    
    matches = matcher(nlp_text)
    for match_id, start, end in matches:
        span = nlp_text[start:end] 
        designation_set.append(span.text)
    return [i for i in set(designation_set) if i.lower() in designations]
    
def extract_text_from_pdf(pdf_path):
    with open(pdf_path, 'rb') as fh:
        for page in PDFPage.get_pages(fh, caching=True, check_extractable=True):
            #creating a resoure manager
            resource_manager = PDFResourceManager()
            #create a file handle
            fake_file_handle = io.StringIO()
            #creating a text converter object
            converter = TextConverter(resource_manager,fake_file_handle, 
                                codec='utf-8', laparams=LAParams())
            #creating a page interpreter
            page_interpreter = PDFPageInterpreter(resource_manager,converter)
            #process current page
            page_interpreter.process_page(page)
            #extract text
            text = fake_file_handle.getvalue()
            yield text
            converter.close()
            fake_file_handle.close()
            
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def removeNonAscii(s):
    return re.sub(r'\\u\w{4}','',s)

@app.route('/',  methods=['GET', 'POST'])
def upload():
    filenames = []
    text =''
    if request.method == 'POST':
        #checking file present in POST request
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        #checking file extention 
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            #saving the pdf to upload folder
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            filenames.append(filename)
            #extracting text from pdf
            for page in extract_text_from_pdf(os.path.join(app.config['UPLOAD_FOLDER'], filename)):
                text += ' ' + page
            text = ' '.join(text.split())
            the_list = re.findall(r'Curriculum Vitae', text,re.IGNORECASE)
            for item in the_list:
                text = text.replace(item,'')
            try:
                #extract necessary information
                user_name = extract_name(text)
                user_name = unidecode(user_name)
                mob_no = extract_mobile_number(text)
                email = extract_email(text)
                skills = extract_skills(text)
                skills = ','.join(skills)
                designations = extract_designations(text)
            except :
                pass
            d = OrderedDict()
            d = {'Name': user_name, 'Mobile Number': mob_no,
                 'Email ID':email,'Skills':skills,'Designations':designations}
            #sending the response
            return jsonify(Result=d)
    return render_template('index.html')

'''
@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename),'got ur cv!!
    return 'got ur cv!!!' '''

if __name__ == "__main__":
    app.run(debug=True)
