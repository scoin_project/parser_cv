import os,io,re
import pandas as pd
from flask import Flask,flash,request,redirect,url_for,render_template,send_from_directory,jsonify,session
from werkzeug.utils import secure_filename
import tika
from tika import parser
import base64
from collections import OrderedDict
import utils
import json
#pdfminer configuration
from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
#spacy configuration
import spacy
from spacy.matcher import Matcher,PhraseMatcher
from unidecode import unidecode
from spacy import displacy
#nltk configuration
import nltk
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize
#nltk.download('stopwords')
#stop_words = set(stopwords.words('english'))

nlp = spacy.load('en_core_web_sm')
matcher = Matcher(nlp.vocab)
UPLOAD_FOLDER = os.path.join(os.getcwd(),"./cvs/")
ALLOWED_EXTENSIONS = set(['pdf'])
output_dir = os.path.join(os.getcwd(),"./model/")

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

def cleanup(token, lower = True):
    if lower:
        token = token.lower()
    return token.strip()

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/',  methods=['GET', 'POST'])
def upload():
    filenames = []
    clean_text = []
    text =''
    if request.method == 'POST':
        #checking file present in POST request
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        #checking file extention 
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            #saving the pdf to upload folder
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            filenames.append(filename)
            #extracting text from pdf
            for page in utils.extract_text_from_pdf(os.path.join(app.config['UPLOAD_FOLDER'], filename)):
                text += ' ' + page
            text = ' '.join(text.split())
            the_list = re.findall(r'Curriculum Vitae',text,re.IGNORECASE)
            for item in the_list:
                text = text.replace(item,'')
            try:
                text = unidecode(text)
                #extract necessary information
                #user_name = utils.extract_name(text)
                #user_name = unidecode(user_name)
                #mob_no = utils.extract_mobile_number(text)
                #email = utils.extract_email(text)
                #skills = utils.extract_skills(text)
                #skills = ','.join(skills)
                #test the saved model
                print("Loading from", output_dir)
                nlp2 = spacy.load(output_dir)
                d = OrderedDict()
                print("Loading text", output_dir)
                doc = nlp2(text)
                #displacy.serve(doc, style="ent")
                print("entities are: ")
                labels = set([w.label_ for w in doc.ents])
                for label in labels:
                    entities = [cleanup(e.string, lower=False) for e in doc.ents if label == e.label_]
                    entities = list(set(entities))
                    print(label, entities)
                    d[label] = entities
            except Exception as e:
                print(e)
            #d = OrderedDict()
            #d = {'Name': user_name, 'Mobile Number': mob_no,
                # 'Email ID':email,'Skills':skills}
            #sending the response back
            return jsonify(Result=d)
            #return displacy.render(doc, style='ent', page=True) 
    return render_template('index.html')
'''
@app.route('/display',  methods=['GET', 'POST'])
def display():
    doc = session['a']
    return displacy.render(doc, style='ent', page=True)'''
        
    
if __name__ == "__main__":
    app.run(host='0.0.0.0',debug=True)
