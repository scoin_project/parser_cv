import json,os,re
import numpy as np
import random
import logging
import spacy
import timeit
from datetime import timedelta
from spacy.util import minibatch, compounding
from spacy.gold import GoldParse
from spacy.scorer import Scorer
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import accuracy_score

train_path = os.path.join(os.getcwd(),"./training_data/",'input_220.json')
test_path = os.path.join(os.getcwd(),"./training_data/",'testdata.json')
output_dir = os.path.join(os.getcwd(),"./model/")
new_model_name = "trained_model"

def trim_entity_spans(data: list) -> list:
    #Removes leading and trailing white spaces from entity spans
    invalid_span_tokens = re.compile(r'\s')

    cleaned_data = []
    for text, annotations in data:
        entities = annotations['entities']
        valid_entities = []
        for start, end, label in entities:
            valid_start = start
            valid_end = end
            while valid_start < len(text) and invalid_span_tokens.match(
                    text[valid_start]):
                valid_start += 1
            while valid_end > 1 and invalid_span_tokens.match(
                    text[valid_end - 1]):
                valid_end -= 1
            valid_entities.append([valid_start, valid_end, label])
        cleaned_data.append([text, {'entities': valid_entities}])

    return cleaned_data

def evaluate(ner_model, examples):
    scorer = Scorer()
    print("Evaluating!!")
    d = {}
    tp,tr,tf,ta,c=0,0,0,0,0
    for input_, annot in examples:
        #print("in first")
        doc_to_test=ner_model(input_)
        for ent in doc_to_test.ents:
            #print("in Second")
            d[ent.label_]=[0,0,0,0,0,0]
        for ent in doc_to_test.ents:
            #print("in third")
            doc_gold_text = ner_model.make_doc(input_)
            gold = GoldParse(doc_gold_text, entities=annot.get("entities"))
            y_true = [ent.label_ if ent.label_ in x else 'Not '+ent.label_ for x in gold.ner]
            y_pred = [x.ent_type_ if x.ent_type_ ==ent.label_ else 'Not '+ent.label_ for x in doc_to_test]
            if(d[ent.label_][0]==0):
                #print("In if")
                (p,r,f,s)= precision_recall_fscore_support(y_true,y_pred,average='weighted',labels=np.unique(y_pred))
                a=accuracy_score(y_true,y_pred)
                d[ent.label_][0]=1
                d[ent.label_][1]+=p
                d[ent.label_][2]+=r
                d[ent.label_][3]+=f
                d[ent.label_][4]+=a
                d[ent.label_][5]+=1
        c+=1
    return d
def convert_json_to_spacy(json_path):
    try:
        training_data = []
        lines = []
        with open(json_path,'r') as f:
            lines = f.readlines()
        for line in lines:
            data = json.loads(line)
            text = data['content']
            entities = []
            for annotation in (data['annotation'] or []):
                if annotation:
                    point = annotation['points'][0]
                    labels = annotation['label']
                    if not isinstance(labels,list):
                        labels = [labels]
                    for label in labels:
                        entities.append((point['start'], point['end'] + 1 ,label))
            training_data.append((text, {"entities" : entities}))
        return training_data
    except Exception as e:
        logging.exception("Unable to process " + json_path + "\n" + "error = " + str(e))
        return None

def train_spacy():
    try:
        TRAIN_DATA = convert_json_to_spacy(train_path)
        TRAIN_DATA = trim_entity_spans(TRAIN_DATA)
        '''TRAIN_DATA = [
                    ('Resume | Aayush Singh\n\nAayush Singh\n', {
                        'entities': [(9, 21, 'Name'), (23, 35, 'Name')]
                    }),
                    ('Email – aayushsinghm16@gmail.com |', {
                        'entities': [(8, 32, 'EmailAddress')]
                    }),
                    ('Tel - +91 9582707868\nGitHub - https://github.com/aayushsinghm16\n', {
                        'entities': []
                    }),
                    ('Linkedin - https://www.linkedin.com/in/aayushsinghm16\n\n', {
                        'entities': []
                    }),
                    ('507 - E2 Tower\nBharat City\n', {
                        'entities': []
                    }),
                    ('Teela More, Loni\nGhaziabad, UP(201102)\n\nEmployment\nFreelancer\nNovember 2016-Present\n', {
                        'entities': [(17, 37, 'Location')]
                    }),
                    ('\uf0b7 Developed and managed Hybrid application using Meteorjs(Nodejs Framework), Html, SASS,\nMongodb, Blaze, Google API and Heroku.\n', {
                        'entities': [(77, 81, 'Skills'), (83, 87, 'Skills'), (89, 97, 'Skills'), (98, 103, 'Skills'), (120, 126, 'Skills')]
                    }),
                    ('\uf0b7 Frontend Development for http://www.mdesiitd.com/ using Angularjs, HTML, CSS.\n', {
                        'entities': [(58, 67, 'Skills'), (67, 73, 'Skills'), (75, 78, 'Skills')]
                    }),
                    ('\uf0b7 Developed http://padl.in/\n', {
                        'entities': []
                    }),
                    ('\uf0b7 Developed https://kaamini.herokuapp.com/\n', {
                        'entities': []
                    }),
                    ('IIT Delhi – Project Assistant\nJune 2015-September 2016\n\uf0b7 Developed and managed web portal of Rural Housing Knowledge Network for Ministry of Rural\nDevelopment.\n', {
                        'entities': [(12, 29, 'Designation')]
                    }),
                    ('\uf0b7 Using Flask (Python Framework), Mongodb, Html, CSS, Javascript, jquery, Sijax, Google Map API.\n', {
                        'entities': [(8, 13, 'Skills'), (15, 21, 'Skills'), (34, 42, 'Skills'), (43, 47, 'Skills'), (49, 52, 'Skills'), (54, 65, 'Skills'), (66, 72, 'Skills'), (74, 79, 'Skills'), (81, 95, 'Skills')]
                    }),
                    ('Certification\n\uf0b7 Java, Ducat 2012\n\uf0b7 Cloud Computing, Ducat 2013\n\nPersonal Details\nDate of Birth\nNationality\n\nFeb 17th, 1991\nIndian\n\nPage 2', {
                        'entities': []
                    })
                    ]'''
        TEST_DATA = convert_json_to_spacy(test_path)
        TEST_DATA = trim_entity_spans(TEST_DATA)

        n_itn = 64
        random.seed(0)
        nlp = spacy.blank('en')
        #create new pipeline component
        if 'ner' not in nlp.pipe_names:
            ner = nlp.create_pipe('ner')
            nlp.add_pipe(ner,last=True)
        else:
            ner = nlp.get_pipe("ner")
        #add labels
        for _,annotations in TRAIN_DATA:
            for ent in annotations.get('entities'):
                ner.add_label(ent[2])
        
        #disable other pipes from pipeline during training
        other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'ner']
        with nlp.disable_pipes(*other_pipes):
            optimizer = nlp.begin_training()
        move_names = list(ner.move_names)
        start = timeit.default_timer()
        for itn in range(n_itn):
            random.shuffle(TRAIN_DATA)
            losses = {}
            # batch up the examples using minibatch
            batches = minibatch(TRAIN_DATA, size=compounding(4.0, 32.0, 1.001))
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(
                    texts,  # batch of texts
                    annotations,  # batch of annotations
                    drop=0.5,  # dropout - make it harder to memorise data
                    losses=losses,
                )
            print("Losses", losses)
        stop = timeit.default_timer()
        print('Time Elapsed:',str(timedelta(seconds=stop - start)))
        #save the model
        if output_dir is not None:
            nlp.meta["name"] = new_model_name
            nlp.to_disk(output_dir)
            print("Saved model to",output_dir)
        print("Loading from", output_dir)
        nlp2 = spacy.load(output_dir)
        # Check the classes have loaded back consistently
        assert nlp2.get_pipe("ner").move_names == move_names  
        #evaluate model'''
        d = evaluate(nlp2, TEST_DATA)
        print("evaluated")
        for i in d:
            print("\n For Entity "+i+"\n")
            print("Accuracy : "+str((d[i][4]/d[i][5])*100)+"%")
            print("Precision : "+str(d[i][1]/d[i][5]))
            print("Recall : "+str(d[i][2]/d[i][5]))
            print("F-score : "+str(d[i][3]/d[i][5]))
        #test the saved model
        #test_text = "Java"
        #doc2 = nlp2(test_text)
        #for ent in doc2.ents:
            #print(ent.label_, ent.text)
    except Exception as e:
            print(e)
train_spacy()
